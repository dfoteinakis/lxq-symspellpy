import pkg_resources
import re
from symspellpy.symspellpy import SymSpell, Verbosity
import diff

def loadDictionary(resource):
    # Set max_dictionary_edit_distance to avoid spelling correction
    # 1 for chinese, 2 for the rest
    sym_spell = SymSpell(max_dictionary_edit_distance=1, prefix_length=7)
    dictionary_path = pkg_resources.resource_filename("symspellpy", resource)
    # term_index is the column of the term and count_index is the
    # column of the term frequency
    sym_spell.load_dictionary(dictionary_path, term_index=0, count_index=1)
    return sym_spell

def checkInput2(input, sym_spell): 
    input_term = re.sub("[\\s+\\.!\\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）：；《）《》“”()»〔〕-]+", "", input)
    print("[checkInput2] input_term:", input_term)
    result = sym_spell.word_segmentation(input_term)
    print("[checkInput2] result: ", result)
    print("[checkInput2] corrected:", result.corrected_string)
    corrected = result.corrected_string.replace(' ', '')
    if corrected != input_term: 
        print("[checkInput2] corrections found")
        warnings = diff.computeDifferences(input_term, corrected)
        return warnings
    else:
        return []

def checkInput3(input, sym_spell): 
    input_term = re.sub("[\\s+\\.!\\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）：；《）《》“”()»〔〕-]+", "", input)
    print("[checkInput3] input_term:", input_term)
    result = sym_spell.word_segmentation(input_term,max_edit_distance=0)
    print("[checkInput3] result: ", result)
    print("[checkInput3] corrected:", result.corrected_string)
    splits = result.segmented_string.split(' ')
    for split in splits:
        suggests_1 = sym_spell.lookup(split, Verbosity.CLOSEST, max_edit_distance=1)
        # suggests_1 = sym_spell.lookup(split, Verbosity.CLOSEST, max_edit_distance=2)
        print("term:", split)
        # print("suggests_0:", suggests_0)
        for suggest in suggests_1:
            print(suggest.term, suggest.distance, suggest.count)
        # print("suggests_1:", suggests_1)
    result = sym_spell.word_segmentation(input_term,max_edit_distance=1)
    print("[checkInput3] result (edit distance 1): ", result)
    print("[checkInput3] corrected (edit distance 1):", result.corrected_string)
    splits = result.segmented_string.split(' ')
    for split in splits:
        suggests_1 = sym_spell.lookup(split, Verbosity.CLOSEST, max_edit_distance=1)
        # suggests_1 = sym_spell.lookup(split, Verbosity.CLOSEST, max_edit_distance=2)
        print("term:", split)
        # print("suggests_0:", suggests_0)
        for suggest in suggests_1:
            print(suggest.term, suggest.distance, suggest.count)
        # print("suggests_1:", suggests_1)
    return ''

def checkInput4(input, sym_spell):
    input_term = re.sub("[\\s+\\.!\\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）：；《）《》“”()»〔〕-]+", "", input)
    print("[checkInput4] input_term:", input_term)
    suggestions = sym_spell.lookup_compound(input_term, 2)
    for suggest in suggestions:
        print(suggest.term, suggest.distance, suggest.count)
    return ''

def checkInput5(input, sym_spell):
    input_term = input
    print("[checkInput5] input_term:", input_term)
    result = sym_spell.word_segmentation(input_term,max_edit_distance=0)
    print("[checkInput5] result: ", result)
    splits = result.segmented_string.split(' ')
    current_index = 0
    warnings = []
    for split in splits:
        suggests_1 = sym_spell.lookup(split, Verbosity.CLOSEST, max_edit_distance=1)
        # print("term:", split)
        found_with_distance_0 = False
        for suggest in suggests_1:
            if suggest.distance == 0:
                found_with_distance_0 = True
                break
        if found_with_distance_0 != True:
            warn = {
                'start': current_index,
                'length': len(split),
                'text': split
            }
            warnings.append(warn)
        current_index = current_index + len(split)
    return warnings

def checkInput(input, sym_spell_bigrams, sym_spell_single):
    # split the cleaned-up segment
    # remove Chinese punctuation, if any
    input_term = re.sub("[\\s+\\.!\\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）：；《）《》“”()»〔〕-]+", "", input)
    print("input_term:", input_term)
    result = sym_spell_bigrams.word_segmentation(input_term)
    print("result: ", result)
    print("corrected:", result.corrected_string)
    result_split = result.corrected_string.split()

    print("{}".format(result_split))
    print(len(result_split), 'words contained in the original split')
    # lookup suggestions for single-word input 
    count = 0
    omitted_words = []

    for word in result_split:
        suggestions = sym_spell_bigrams.lookup(word, Verbosity.CLOSEST, max_edit_distance=0)

        if suggestions == []:
            omitted_words.append(word)
        else:
            for suggestion in suggestions:
                # print('suggestion, edit distance, term frequency')
                print(suggestion)
                count = count + 1

    print('----------------------------------------------------------')
    print(count, '/', len(result_split), 'candidates')
    print(len(omitted_words), '/', len(result_split), 'omitted words below')
    print('----------------------------------------------------------')
    # now go through the omitted words and break them further to see if there is any match
    for w in omitted_words:
        print('omitted word', w, 'is now resplit based on another dictionary:')
        res = sym_spell_single.word_segmentation(w)
        res_split = res.segmented_string.split()
        print(res_split)
    




# sym_spell_bigrams = loadDictionary("zh_bigrams_no_colon.txt")

# sym_spell_single = loadDictionary("zh-cn-global.txt")
sym_spell_single = loadDictionary("zh-cn-freq-combined.txt")

sym_spell_bigrams = loadDictionary("Bigram2.txt")

# sym_spell_single.load_bigram_dictionary(pkg_resources.resource_filename("symspellpy", "zh-fotd-bigram.ext"),0,2)

# suggests = sym_spell_single.lookup('感谢',Verbosity.TOP)
# for suggest in suggests:
#     print('感谢', suggest.term, suggest.distance, suggest.count)
# suggests = sym_spell_single.lookup('您',Verbosity.TOP)
# for suggest in suggests:
#     print('您', suggest.term, suggest.distance, suggest.count)

# a sentence without any spaces
# CORRECT
# input_term = '感谢您对与维基百科取得联络的兴趣。在您开始之前，请阅读本页的重要信息：'
# input_term = '感谢您对与维基百科取得联络的兴趣'
# print('CORRECT')
# input_term = '一百美元等于多少人民币'
# warns = checkInput5(input_term, sym_spell_single)
# print(warns)

print('CORRECT - single')
input_term = '感谢您对与维基百科取得联络的兴趣'
warns = checkInput5(input_term, sym_spell_single)
print(warns)
print('CORRECT - bigrams')
input_term = '感谢您对与维基百科取得联络的兴趣'
warns = checkInput5(input_term, sym_spell_bigrams)
print(warns)
# checkInput(input_term, sym_spell_bigrams, sym_spell_single)
# WRONG VERSION OF THE PREVIOUS SEGMENT
# input_term = '敢谢您等与威基百抖取得连烙的性趣。于你开示之前，请您约都本叶的重腰信任：' 
print('WRONG - single')
input_term = '敢谢您等与威基百抖取得连烙的性趣'
warns = checkInput5(input_term, sym_spell_single)
print(warns)
print('WRONG - bigrams')
input_term = '敢谢您等与威基百抖取得连烙的性趣'
warns = checkInput5(input_term, sym_spell_bigrams)
print(warns)
# # # # checkInput(input_term, sym_spell_bigrams, sym_spell_single)
# print('CORRECT')
# input_term = '在您开始之前'
# warns = checkInput5(input_term, sym_spell_single)
# print(warns)

# print('WRONG')
# input_term = '于你开示之前'
# warns = checkInput5(input_term, sym_spell_single)
# print(warns)

print('CORRECT - single')
input_term = '请阅读本页的重要信息'
warns = checkInput5(input_term, sym_spell_single)
print(warns)
print('CORRECT - bigrams')
input_term = '请阅读本页的重要信息'
warns = checkInput5(input_term, sym_spell_bigrams)
print(warns)
# print('WRONG')
# input_term = '请您约都本叶的重腰信任'
# warns = checkInput5(input_term, sym_spell_single)
# print(warns)

print('WRONG - single')
input_term = '以下分类页面实际存在'
warns = checkInput5(input_term, sym_spell_single)
print(warns)
print('WRONG - bigrams')
input_term = '以下分类页面实际存在'
warns = checkInput5(input_term, sym_spell_bigrams)
print(warns)
# print('WRONG')
# input_term = '即使没有其它页面或分类利用它'
# warns = checkInput5(input_term, sym_spell_single)
# print(warns)