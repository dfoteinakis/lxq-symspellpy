### Chinese frequency words dictionary from hermitdave's repo (including single characters)

https://raw.githubusercontent.com/hermitdave/FrequencyWords/master/content/2018/zh_tw/zh_tw_full.txt

### Bigram dictionary (separated with colon - removed in our repo)

https://raw.githubusercontent.com/xujiajun/gotokenizer/master/data/zh/bigram.txt
