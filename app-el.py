import pkg_resources
from symspellpy.symspellpy import SymSpell, Verbosity

# Set max_dictionary_edit_distance to avoid spelling correction
sym_spell = SymSpell(max_dictionary_edit_distance=2, prefix_length=7)

dictionary_path = pkg_resources.resource_filename(
    "symspellpy", "el_full.txt")

# term_index is the column of the term and count_index is the
# column of the term frequency
sym_spell.load_dictionary(dictionary_path, term_index=0, count_index=1)

# a sentence without any spaces
# input_sentence = 'Εχτές πήγαο για να φάω τιρόπιτα σε αίνα εληνικό μαγαζζί, τον Γριγόρη.' 
input_term = 'μαγαζίί'

suggestions = sym_spell.lookup(input_term, Verbosity.CLOSEST,
                               max_edit_distance=2, include_unknown=True)

# display suggestion term, term frequency, and edit distance
for suggestion in suggestions:
    print(suggestion)
    print(type(suggestion))