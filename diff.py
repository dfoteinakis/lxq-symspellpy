import difflib
x = '敢谢您等与威基百抖取得连烙的性趣'
a = '感谢您对与维基百科取得联络的兴趣'
b = '感谢您维基百科取得联络的兴趣'

def computeDifferences(a,b):
    s = difflib.SequenceMatcher(None, a, b)
    a_index = 0
    b_index = 0
    print('a:', a)
    print('b:', b)
    warnings = []
    for block in s.get_matching_blocks():
        print(block)
        print('a_index:', a_index, 'block.a:', block.a)
        print('b_index:', b_index, 'block.b:', block.b)
        warn = {}
        # Match(a=0, b=0, size=3)
        # Match(a=5, b=3, size=11)
        # Match(a=16, b=14, size=0)
        if a_index != block.a:
            warn['start'] = a_index
            warn['end'] = block.a
            warn['suggestion'] = b[b_index:block.b]
            warn['text'] = a[a_index:block.a]
            print(warn)
            warnings.append(warn)
        a_index = block.a + block.size
        b_index = block.b + block.size
    return warnings

# computeDifferences(x,a)
# computeDifferences(a,a)